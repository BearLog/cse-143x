package Spec_07;

import java.util.*;
/*
 Thomas Bernardi
 11/11/16
 CSE143X
 TA: Caitlin E. Schaefer
 Assignment #7: Evil Hangman

 Given a dictionary, a length and a max # guesses, reduces dictionary to possible
 words of given length. Every time record is called will categorize words in
 dictionary to families with different patterns including that given letter and 
 choose the largest one, reducing the dictionary to that set and sets pattern to 
 corresponding pattern. If the guess is not contained in this set guessesLeft is 
 decremented. Guess is added to guesses. Gives access to dictionary, guessesLeft,
 guesses (already passed to record) and the current pattern.*/

public class HangmanManager {

    private String pattern;
    private int guessesLeft;
    private Set<String> dictionary;
    private SortedSet<Character> guesses;

    //Populates dictionary with words in given dictionary whose legnths are
    //equal to the given length. Initializes the pattern to an empty pattern
    //with given length and sets guessesLeft to given max
    public HangmanManager(List<String> dictionary, int length, int max) {
        if (length < 1 || max < 0) {
            throw new IllegalArgumentException("Given length must be at least "
                    + "one and max must be at least zero.");
        }

        this.dictionary = new TreeSet<>();
        Iterator<String> it = dictionary.iterator();
        while (it.hasNext()) {
            String next = it.next();
            if (next.length() == length) {
                this.dictionary.add(next);
            }
        }

        this.pattern = "-";
        for (int i = 1; i < length; i++) {
            this.pattern += " -";
        }

        this.guessesLeft = max;
        this.guesses = new TreeSet<>();
    }

    //returns all words being considered by the manager, all that follow this.pattern
    public Set<String> words() {
        return this.dictionary;
    }

    //returns the number of wrong guesses the user has left
    public int guessesLeft() {
        return guessesLeft;
    }

    //returns a set with letters that have been guessed so far
    public SortedSet<Character> guesses() {
        return guesses;
    }

    //returns the current pattern being considered by the manager
    public String pattern() {
        if (this.dictionary.isEmpty()) {
            throw new IllegalStateException("No words in dictionary, impossible"
                    + " to form pattern.");
        }
        return this.pattern;
    }

    //given a char guess, considers all possible patterns that can be made in
    //given the current this.pattern and sets this.pattern to the one whose word
    //"family" has the most words. Sets this.dictionary to that set of words.
    //Returns the number of times guess occurs in the newly selected pattern.
    public int record(char guess) {
        if (this.guessesLeft < 1) {
            throw new IllegalStateException("No more guesses left.");
        }
        if (!this.dictionary.isEmpty() && guesses.contains(guess)) {
            throw new IllegalArgumentException("Already guessed that.");
        }

        Map<String, Set<String>> possiblePatterns = generatePatterns(guess);
        this.pattern = evilestPattern(possiblePatterns, guess);
        this.dictionary = possiblePatterns.get(this.pattern);

        //determine # of occurrences of guess in pattern
        int result = 0;
        for (int i = 0; i < this.pattern.length(); i++) {
            if (this.pattern.charAt(i) == guess) {
                result++;
            }
        }

        //decrement guessesLeft only if the guess was wrong
        if (result == 0) {
            --this.guessesLeft;
        }

        this.guesses.add(guess);
        return result;
    }

    //Given map with String and a set of Strings along with char guess, adds
    //words from the dictionary to the set corresponding to the pattern they
    //match and then returns the pattern whose set of words is the largest.
    private String evilestPattern(Map<String, Set<String>> possible, char guess) {
        Iterator<String> it = dictionary.iterator();

        //for each word in dictionary, add to corresponding pattern "family"
        while (it.hasNext()) {
            String word = it.next();
            Iterator<String> it2 = possible.keySet().iterator();
            while (it2.hasNext()) {
                String temp = it2.next();
                if (comparePattern(word, temp, guess)) {
                    possible.get(temp).add(word);
                }
            }
        }

        Iterator<String> it3 = possible.keySet().iterator();
        String pattern = it3.next();

        //Sets pattern to the pattern with the most matching words
        while (it3.hasNext()) {
            String next = it3.next();
            if (possible.get(pattern).size()
                    < possible.get(next).size()) {
                pattern = next;
            }
        }
        return pattern;
    }

    //returns true if the given word matches the given pattern with respect
    //to the given guess
    private boolean comparePattern(String word, String pattern, char guess) {
        for (int i = 0; i < pattern.length(); i += 2) {
            if ((pattern.charAt(i) != '-'
                    && word.charAt(i / 2) != pattern.charAt(i))
                    || (pattern.charAt(i) == '-' && word.charAt(i / 2) == guess)) {
                return false;
            }
        }
        return true;
    }

    //given a guess, will return a map with all possible modifications to current
    //this.patterns (including no mod) as keys and an empty set of Strings as
    //all values.
    private Map<String, Set<String>> generatePatterns(char guess) {
        Map<String, Set<String>> patterns = new TreeMap<>();
        patterns.put(this.pattern, new TreeSet<>());
        for (int i = 0; i < this.pattern.length(); i += 2) {
            Iterator<String> it = patterns.keySet().iterator();
            Map<String, Set<String>> newPatterns = new TreeMap<>();
            while (it.hasNext()) {
                String pattern = it.next();
                if (pattern.charAt(i) == '-') {
                    String newPattern = pattern.substring(0, i) + guess
                            + pattern.substring(i + 1);
                    newPatterns.put(newPattern, new TreeSet<>());
                }
            }
            patterns.putAll(newPatterns);
        }
        return patterns;
    }
}
