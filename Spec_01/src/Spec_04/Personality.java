


import java.io.*;
import java.util.*;

/*
Thomas Bernardi
10/21/16
CSE143X
TA: Caitlin E. Schaefer
Assignment #4: Personality Test

This class will take an input file, whose name is specified by the user, and convert it into an
output file summarizing the input file. The input contains a list of answers from a personality test
and this program takes that raw data and determines quantitative data (the percentage of each type 
of question answered one way or another) and qualitative data (what personality types those 
percentages map to. These results will be sent to an output file whose name is also specified by the 
user.*/
public class Personality {
    public static final int NUM_DIMENSIONS = 4;
    public static final char[] A_RESULT = {'E', 'S', 'T', 'J'};
    public static final char[] B_RESULT = {'I', 'N', 'F', 'P'};
    
    public static void main(String [] args) throws FileNotFoundException {
        giveIntro();
        
        Scanner console = new Scanner(System.in);     
        
        System.out.print("input file name? ");
        Scanner input = new Scanner(new File(console.nextLine()));
        
        System.out.print("output file name? ");
        PrintStream output = new PrintStream(new File(console.nextLine()));
        
        //Computes and writes results for every person in the input file
        while (input.hasNextLine()){
            String name = input.nextLine();
            String answers = input.nextLine();
            int[] sideA = new int[NUM_DIMENSIONS];
            int[] sideB = new int[NUM_DIMENSIONS];
            
            getCounts(answers, sideA, sideB);
            writeResult(toPercentB(sideA, sideB), name, output);
        }
    }
    
    /*Given a String name (of one individual), an int array percentB with the percent of each type 
    of question they answered "b" and a PrintStream, this method will print to the PrintStream the 
    name, followed by the percentages and finally what those percentages map to qualitatively (in
    terms of personality type).*/
    public static void writeResult(int[] percentB, String name, PrintStream out){
        
        String result = "";
        
        for (int i = 0; i < NUM_DIMENSIONS; i++){
            if (percentB[i] == 50) result += "X";
            else if (percentB[i] < 50) result += A_RESULT[i];
            else if (percentB[i] > 50) result += B_RESULT[i];
        }
        
        out.println(name + ": " + Arrays.toString(percentB) + " = " + result);
        
    }
    
    /*Given two int arrays sideA and sideB which contain the number of A answers and B answers, 
    respectively, for each personality dimension and returns an array, of the same size, with the 
    percent of questions whose answers were B for each dimension4.*/
    public static int[] toPercentB(int[] sideA, int[] sideB){
        int results[] = new int[NUM_DIMENSIONS];
        for (int i = 0; i < NUM_DIMENSIONS; i ++){
            double b = (double) sideB[i];
            double total = (double) (sideA[i] + sideB[i]);
            results[i] = (int) Math.round(100 * b / total);
        }
        return results;
    }
    
    /*Given a string with all of an individual's answers, and two arrays sideA and sideB, each with
    one index per personality dimension, fills sideA with number of A answers for each dimension and
    sideB with number of B answers for each dimension.*/
    public static void getCounts(String answers, int[] sideA, int[] sideB){
        //The first dimension only includes one answer per group of 7
        sideA[0] += checkMatch(0, answers, "a");
        sideB[0] += checkMatch(0, answers, "b");
        //The last 3 dimensions include two answers per group of seven                
        for (int i = 1; i < NUM_DIMENSIONS; i++){
            sideA[i] += checkMatch(2 * i - 1, answers, "a") + checkMatch(2 * i, answers, "a");
            sideB[i] += checkMatch(2 * i - 1, answers, "b") + checkMatch(2 * i, answers, "b");
        }
        
    }
    
    /*Given an int position within the repeating group of seven, a String with all of an 
    individual's answers, and a String match, checks at given position for all ten iterations of the
    7 question pattern to see if the character in the string at that position matches the given 
    "match" string. Returns the number of matches.*/
    public static int checkMatch(int position, String answers, String match){
        int result = 0;
        for (int i = 0; i < 10; i++){
            String answer = answers.substring(7 * i + position, 7 * i + position + 1).toLowerCase();
            if (answer.equals(match)) result += 1;
        }
        return result;
    }
    
    /*Introduces the program to the user and gives context.*/
    public static void giveIntro(){
        System.out.println("This program processes a file of answers to the");
        System.out.println("Keirsey Temperament Sorter. It converts the");
        System.out.println("various A and B answers for each person into");
        System.out.println("a sequence of B-percentages and then into a");
        System.out.println("four-letter personality type.");
        System.out.println();
        
    }
    
}
