


import java.util.*;

/*
Thomas Bernardi
10/14/16
CSE143X
TA: Caitlin E. Schaefer
Assignment #3: Bagels

This program allows a user to play multiple games in which they try to guess a randomly determined
number in as few tries as possible. Once the user decides to stop playing they will be given stats: 
number of games played, total number of guesses, average guesses per game and the number of guesses 
they took in their best game. For each game the user chooses how many digits will be in the number.
Then they start guessing. For each correct digit in the correct place they are told "fermi". For
each correct digit in an incorrect spot they are told "pica". If there are no correct digits they
told "bagels".*/
public class Bagels {
    public static void main(String[] args){
        giveIntro();
        Scanner console = new Scanner(System.in);
        String playAgain = "y";
        int totalGames = 0;
        int totalGuesses = 0;
        int bestGame = 9999;
        
        //Continues gameplay until the answer no longer begins with "y" or "Y" (assumed only other 
        //input in "n" or "N")
        while(playAgain.equals("y")||playAgain.equals("Y")){
            System.out.println();
            System.out.print("How many digits this time? ");
            
            int guesses = playGame(console, console.nextInt());
            if (guesses == 1) System.out.println("You got it right in 1 guess");
            else System.out.println("You got it right in " + guesses + " guesses");
            
            totalGames++;
            bestGame = Math.min(bestGame, guesses);
            totalGuesses += guesses;
            
            System.out.print("Do you want to play again? ");
            playAgain = console.next().substring(0, 1);
        }
        
        printStats(totalGames, totalGuesses, bestGame);
    }
    
    /*Handles gameplay. Given numDigits (number of digits to be guessed), it will create a random
    string of digits 1-9 inclusive of length numDigits. Then it will prompt the user for their guess
    and compare the guess to the randomly created answer. While the answer and guess do not match it
    will continue this process. Once the guess is correct it will return the number of guesses used
    to wherever it was called.*/
    public static int playGame(Scanner console, int numDigits){
        String answer = "";
        boolean correct = false;
        int numGuesses = 0;
        
        Random r = new Random();
        
        for(int i = 0; i < numDigits; i++){
            answer += r.nextInt(9) + 1;
        }
        
        while(!correct){
            System.out.print("Your guess? ");
            correct = checkMatch(answer, console.next().substring(0,numDigits), numDigits);
            numGuesses++;
        }
        
        return numGuesses;
    }
    
    /*Given an answer and a guess, this method compares the two and returns hints as described in
    class comment. Once any necessary hints are given, the method returns true of the guesses match
    and false if the don't.*/
    public static boolean checkMatch(String answer, String guess, int length){
        String response = "";
        
        if(answer.equals(guess)) return true;
        
        for (int i = 0; i < length; i++){
            if(answer.substring(i, i + 1).equals(guess.substring(i, i+1))){
                response += "fermi ";
                answer = replace(answer, i, "X");
                guess = replace(guess, i, "X");
            }
        }
        
        for (int i = 0; i < length; i++){
            for (int j = 0; j < length; j++){
                if(!answer.substring(i, i + 1).equals("X") &&
                        answer.substring(i, i + 1).equals(guess.substring(j, j + 1))){
                    response += "pica ";
                    answer = replace(answer, i, "X");
                    guess = replace(guess, j, "X");
                }
            }
        }
        
        if (response.equals("")) System.out.println("bagels");
        else System.out.println(response);
        
        return false;
    }
    
    /*Prints intro statement*/
    public static void giveIntro(){
        System.out.println("Welcome to CSE 143x Bagels!");
        System.out.println("I'll think up a number for you to guess.");
        System.out.println("Each digit will be between 1 and 9.");
        System.out.println("Try to guess my number, and I'll say \"fermi\"");
        System.out.println("for each digit you get right and in the right");
        System.out.println("place, and \"pica\" for each digit you get right");
        System.out.println("that is in the wrong place.");
    }
    
    /*Given totalGames, totalGuesses and bestGame, prints out these stats and calculates guesses per
    game and prints that out too.*/
    public static void printStats(int totalGames, int totalGuesses, int bestGame){
        System.out.println();
        System.out.println("Overal results:");
        System.out.println("    total games   = " + totalGames);
        System.out.println("    total guesses = " + totalGuesses);
        System.out.println("    guesses/game  = " + 
                round1((double)totalGuesses/(double)totalGames));
        System.out.println("    best game     = " + bestGame);
    }
    
    // returns the string obtained by replacing the character at the given
    // index with the replacement text
    public static String replace(String s, int index, String replacement) {
        return s.substring(0, index) + replacement + s.substring(index + 1);
    }
        
    // returns the result of rounding n to 1 digit after the decimal point
    public static double round1(double value) {
        return (int) (10 * value + 0.5) / 10.0;
    } 
}
