
package Spec_05;

import java.awt.*;
/*
 Thomas Bernardi
10/28/16
CSE143X
TA: Caitlin E. Schaefer
Assignment #5: Critters -- Giant

Giants will always infect if there is something else in front. Otherwise they
will turn right unless the space in front of them is unoccupied. The string they
return changes between "fee" "fie" "foe" and "fum" every six moves and they
always return gray as their color.
*/
public class Giant extends Critter{
    int move;
    
    //Init & set # moves to zero
    public Giant(){
        move = 0;
    }
    
    //Given a CritterInfo, returns behavior described in class comment
    public Action getMove(CritterInfo info){
        move++;
        if(info.getFront() == Neighbor.OTHER){
            return Action.INFECT;
        }else if(info.getFront() == Neighbor.EMPTY){
            return Action.HOP;
        }else{
            return Action.RIGHT;
        }
    }
    
    public Color getColor(){
        return Color.GRAY;
    }
    
    //Alternates returing "fee" "fie" "foe" and "fum" every six moves
    public String toString(){
        if (move % 24 < 6){
            return "fee";
        }else if (move % 24 < 12){
            return "fie";
        }else if (move % 24 < 18){
            return "foe";
        }else{
            return "fum";
        }
    }
}
