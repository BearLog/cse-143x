package Spec_05;

import java.awt.*;
import java.util.*;
/*
Thomas Bernardi
10/29/16
CSE143X
TA: Caitlin E. Schaefer
Assignment #5: Critters -- Lion

Lion has a random color between red, blue and green, changing to a new color
every three moves. Infects if OTHER is in front of it, otherwise it turns left
if there is a wall to the front or right, otherwise it turns right if there is
another lion in front of it, otherwise it just hops. It returns an "L" as its
String.
 */
public class Lion extends Critter{
    Color color;
    Random random;
    int move;
    
    //Sets number of moves to zero
    public Lion(){
        move = 0;
        random = new Random();
    }
    
    //Given a CritterInfo, behaves as described in class comment
    public Action getMove(CritterInfo info){
        move++;
        if(info.getFront() == Neighbor.OTHER){
            return Action.INFECT;
        } else if (info.getFront() == Neighbor.WALL || info.getRight() 
                == Neighbor.WALL){
            return Action.LEFT;
        } else if (info.getFront() == Neighbor.SAME){
            return Action.RIGHT;
        } else {
            return Action.HOP;
        }
    }
    
    //Sets current color field to a random color between red, green and blue if 
    //it is at a move that is a multiple of 3, then returns the current color
    public Color getColor(){
        if (move % 3 == 0){
            int num = random.nextInt(3);
            if (num == 0){
                color = Color.RED;
            } else if (num == 1){
                color = Color.GREEN;
            } else {
                color = color.BLUE;
            }
        }
        return color;
    }
   
    //Returns "L" (I feel this is superfluous...)
    public String toString(){
        return "L";
    }
}
