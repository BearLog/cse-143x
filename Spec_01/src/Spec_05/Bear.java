package Spec_05;

import java.awt.*;

/*
Thomas Bernardi
10/28/16
CSE143X
TA: Caitlin E. Schaefer
Assignment #5: Critters -- Bear

Bear can either be polar (displayed as white) or not (displayed black). It will
infect if there is an OTHER neighbor in front, HOP if there is nothing in front
if it or else it will turn left. It's display alternates between a forward slash
and a back slash each turn.
 */
public class Bear extends Critter{
    boolean polar;
    int move;
    
    //Sets polar to the value passed to the constructor and sets move to zero
    public Bear(boolean polar){
        this.polar = polar;
        move = 0;
    }
    
    //See class comment for behavior
    public Action getMove(CritterInfo info){
        move++;
        if (info.getFront() == Neighbor.OTHER){
            return Action.INFECT;
        } else if (info.getFront() == Neighbor.EMPTY){
            return Action.HOP;
        } else {
            return Action.LEFT;
        }
    }
    
    //Returns white if bear is polar otherwise returns white
    public Color getColor(){
        if(this.polar){
            return Color.WHITE;
        }else{
            return Color.BLACK;
        }
    }
    
    //Alternates between forward slash and back slash
    public String toString(){
        if(move % 2 == 0){
            return "/";
        }else{
            return "\\";
        }
    }
}
