package Spec_05;

import java.awt.*;
/*
Thomas Bernardi
10/28/16
CSE143X
TA: Caitlin E. Schaefer
Assignment #5: Critters -- Husky

Husky always returns string "H" and color red. It will always infect if OTHER is
in front. Otherwise, if it determines it is in any type of corner it will
alternate between facing north and west. Otherwise, it will tend to the south
and east.
 */
public class Husky extends Critter{
        
    public Husky(){        
        
    }
    
    //Given CritterInfo, infects if OTHER in front, otherwise alternates between
    //facing north and east if in corner, tends to the south and east
    public Action getMove(CritterInfo info){
        if (info.getFront() == Neighbor.OTHER){
            return Action.INFECT;
        }else if(inCorner(info)){
            if (info.getDirection() == Direction.WEST || 
                    info.getDirection() == Direction.NORTH){
                return Action.RIGHT;
            }else{
                return Action.LEFT;
            }
        }else if (info.getDirection() == Direction.NORTH){
            return Action.LEFT;
        }else if (info.getDirection() == Direction.SOUTH &&
                info.getFront() == Neighbor.WALL){
            return Action.RIGHT;
        }else if (info.getDirection() == Direction.EAST){
            return Action.RIGHT;
        }else if (info.getDirection() == Direction.WEST && 
                info.getFront()== Neighbor.WALL){
            return Action.LEFT;
        }else{
            return Action.HOP;
        }
    }
    
    public Color getColor(){
        return Color.RED;
    }
    
    public String toString(){
        return "H";
    }
    
    //Given CritterInfo this will return true if at least two of the neighbors
    //are not empty
    private boolean inCorner(CritterInfo info){
        int numNeighbors = 0;
        if (info.getFront() != Neighbor.EMPTY){
            numNeighbors++;
        }
        if (info.getLeft() != Neighbor.EMPTY){
            numNeighbors++;
        }
        if (info.getBack() != Neighbor.EMPTY){
            numNeighbors++;
        }
        if (info.getRight() != Neighbor.EMPTY){
            numNeighbors++;
        }
        
        return numNeighbors > 1;
    }
}
