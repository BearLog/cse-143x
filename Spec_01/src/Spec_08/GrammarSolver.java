package Spec_08;

import java.util.*;
/**
 *
 * Thomas Bernardi
 * 11/18/16
 * CSE143X
 * TA: Caitlin E. Schaefer
 * Assignment #8: GrammarSolver
 *
 *Takes a grammar in BNF and randomly generates valid instances of any specified
 * non-terminal symbol from the grammar.
 */
public class GrammarSolver {
    
    private SortedMap<String, List<String[]>> grammar;
    private Random random;
    
    //Throws exception if the given grammar has no rules
    //Throws exception if there are multiple definitions of the same non-terminal
    public GrammarSolver(List<String> grammar){
        if(grammar.isEmpty()){
            throw new IllegalArgumentException("Grammar must have at least one"
                    + "element");
        }
        
        this.grammar = new TreeMap<>();
        random = new Random();
        
        for (String symbol : grammar){
            String[] nonterminal = symbol.split("::=");
            if(this.grammar.containsKey(nonterminal[0])){
                throw new IllegalArgumentException("Grammar may only have one"
                        + "set of rules per non-terminal.");
            }
            
            String[] rules = nonterminal[1].split("[|]");
            List<String[]> rulesSet = new ArrayList<>();
            for(String rule : rules){
                rulesSet.add(rule.trim().split("[ \t]+"));
            }
            this.grammar.put(nonterminal[0], rulesSet);
        }
    }
    
    //returns true if the grammar contains the symbol as a non-terminal
    public boolean grammarContains(String symbol){
        return grammar.containsKey(symbol);
    }
    
    //generates valid instances of the given symbol within the grammar times
    //number of times
    //Throws exception if the grammar doesn't contain the given symbol as a 
    //non-terminal
    //Throws exception if times is less than zero
    public String[] generate(String symbol, int times){
        if (!grammar.containsKey(symbol)){
            throw new IllegalArgumentException("Given symbol not contained in "
                    + "grammar.");
        }
        if (times < 0){
            throw new IllegalArgumentException("Cannot generate symbol a"
                    + "negative number of times. Please provide non-negative"
                    + "number.");
        }
        String[] result = new String[times];
        String[] symbols = {symbol};
        for (int i = 0; i < times; i++){
            result[i] = generate(symbols).trim();
        }
        return result;
    }
    
    //generates valid instance of given symbols within the grammar
    private String generate(String[] symbols){
        String result = "";
        for (String symbol : symbols){
            if (grammarContains(symbol)){
                List<String[]> rule = grammar.get(symbol);
                result += generate(rule.get(random.nextInt(rule.size())));
            }else{
                result += " " + symbol;
            }
        }
        return result;
    }
    
    //returns all non-terminal symbols within grammar
    public String getSymbols(){
        return grammar.keySet().toString();
    }
}
