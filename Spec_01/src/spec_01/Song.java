package spec_01;



/*Thomas Bernardi
10/3/16
CSE143X
TA: CAITLIN E. SCHAEFER
Assignment 1: Song
*/

public class Song {


    public static void main(String[] args) {
        verse01();
        verse02();
        verse03();
        verse04();
        verse05();
        verse06();
        verse07();
    }
    
    //each verse calls the previous one so the song builds on itself
    
    public static void refrain(){
        System.out.println("I don't know why she swallowed that fly,");
        System.out.println("Perhaps she'll die.");
        System.out.println();
    }
    
    public static void verse01(){
        System.out.println("There was an old woman who swallowed a fly.");
        refrain();
    }
    
    public static void verse02(){
        System.out.println("There was an old woman who swallowed a spider,");
        System.out.println("That wriggled and iggled and jiggled inside her.");
        spider();
    }
    
    public static void verse03(){
        System.out.println("There was an old woman who swallowed a bird,");
        System.out.println("How absurd to swallow a bird.");
        bird();
    }
    
    public static void verse04(){
        System.out.println("There was an old woman who swallowed a cat,");
        System.out.println("Imagine that to swallow a cat.");
        cat();
    }
    
    public static void verse05(){
        System.out.println("There was an old woman who swallowed a dog,");
        System.out.println("What a hog to swallow a dog.");
        dog();
    }
    
    public static void verse06(){
        System.out.println("There was an old woman who swallowed a laser,");
        System.out.println("A daring feat that didn't even phase her.");
        laser();
    }
    
    public static void verse07(){
        System.out.println("There was an old woman who swallowed a horse,");
        System.out.println("She died of course.");
    }
    
    public static void spider(){
        System.out.println("She swallowed the spider to catch the fly,");
        refrain();
    }
    
    public static void bird(){
        System.out.println("She swallowed the bird to catch the spider,");
        spider();
    }
    public static void cat(){
        System.out.println("She swallowed the cat to catch the bird,");
        bird();
    }
    
    public static void dog(){
        System.out.println("She swallowed the dog to catch the cat,");
        cat();
    }
    
    public static void laser(){
        System.out.println("She swallowed the laser to neutralize the dog,");
        dog();
    }
}
