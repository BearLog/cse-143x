package Spec_06;

import java.util.*;
/*
 Thomas Bernardi
 11/3/16
 CSE143X
 TA: Caitlin E. Schaefer
 Assignment #6: Assassin Manager

 Keeps track of two lists, a killRing and a graveyard. Sets up a killRing on
 construction with a given list that describes all kill relationships for it. Can
 print out the killRing, or graveyard. Can tell if the killRing or graveyard 
 contains a name, if the game is over and who the winner is (provided there is
 one. Finally, its kill method will move the killed person from the killRing
 to the graveyard as well as keep track of who killed that person.
 */

public class AssassinManager {

    private AssassinNode killingFront; //front of the killRing
    private AssassinNode graveyardFront; //front of the graveyard

    //Add all elements in given list names to a linked list whose first element
    //is killingFront. Points last node to killingFront. Throws
    //IllegalArgumentException if list is empty
    public AssassinManager(List<String> names) {
        if (names.isEmpty()) {
            throw new IllegalArgumentException("List must have at least one "
                    + "element.");
        } else {
            killingFront = new AssassinNode(names.get(0));
            if (names.size() == 1) {
                killingFront.next = killingFront;
            } else {
                AssassinNode temp = new AssassinNode(names.get(names.size() - 1),
                        killingFront);
                for (int i = names.size() - 2; i > 0; i--) {
                    temp = new AssassinNode(names.get(i), temp);
                }
                killingFront.next = temp;
            }
        }

    }

    //Prints out relationship between nodes in list starting with killingFront
    //in the form of node1 "is killing" node2 when node1 is pointing at node2
    public void printKillRing() {
        AssassinNode current = killingFront;
        do {
            System.out.println("    " + current.name + " is stalking "
                    + current.next.name);
            current = current.next;
        } while (!current.name.equals(killingFront.name));
    }

    //Prints who was killed by whom for every element in the graveyard
    public void printGraveyard() {
        AssassinNode current = graveyardFront;
        while (current != null) {
            System.out.println("    " + current.name + " was killed by "
                    + current.killer);
            current = current.next;
        }
    }

    //Given a String name, will return true if that name exists in list starting
    //with killingFront
    public boolean killRingContains(String name) {
        AssassinNode current = killingFront;
        do {
            if (current.name.equals(name)) {
                return true;
            } else {
                current = current.next;
            }
        } while (!current.name.equals(killingFront.name));
        return false;
    }

    //Given a String name, will return true if that name exists in list starting
    //with graveyardFront
    public boolean graveyardContains(String name) {
        AssassinNode current = graveyardFront;
        while (current != null) {
            if (current.name.equals(name)) {
                return true;
            } else {
                current = current.next;
            }
        }
        return false;
    }

    //Returns true if only one element in list starting with killingFront
    public boolean gameOver() {
        return killingFront.next.equals(killingFront);
    }

    //Returns the name in killingFront if it is the only node in the list it
    //starts
    public String winner() {
        if (gameOver()) {
            return killingFront.name;
        } else {
            return null;
        }
    }

    //Removes the node from the killRing whose name matches the given String
    //name and places it at the front of the graveYard, changing it's killer
    //field from null to the name of the node that used to point at it. Throws
    //an IllegalStateException if the game is over and returne an
    //IllegalArgumentException if the given name does not exist in the kill ring.
    public void kill(String name) {
        if (gameOver()) {
            throw new IllegalStateException("Game is over so kill method cannot"
                    + " be called.");
        } else {
            AssassinNode current = killingFront;

            while (!current.next.name.equals(name)) {
                current = current.next;
                if (current.name.equals(killingFront.name)) {
                    //gone all the way through list w/out finding name
                    throw new IllegalArgumentException("Given name does not "
                            + "exist in kill ring.");
                }
            }

            //front case
            if (current.next.equals(killingFront)) {
                killingFront = current.next.next;
            }

            AssassinNode dead = current.next;
            dead.killer = current.name;
            current.next = current.next.next;
            dead.next = graveyardFront;
            graveyardFront = dead;
        }

    }
}
