/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Spec_02;

import java.awt.*;

// Thomas Bernardi
// 10/7/16
// CSE143X
// TA: Caitlin E. Schaefer
// Assignment #2: Doodle

//This assignment is being graded on external correctness not internal correctness
public class Doodle {
    public static void main(String[] args){
        DrawingPanel panel = new DrawingPanel(400,200);
        panel.setBackground(Color.white);
        Graphics g = panel.getGraphics();  
        drawDuke(50,10,200,g);        
    }
    
    public static void drawDuke(int x, int y, int scale, Graphics g){
        g.setColor(Color.BLACK);
        g.drawPolygon(new int[] {x + scale/10, x + scale/2, x + scale * 9 / 10}, 
                new int[] {y + scale * 9 / 10, y + scale / 10, y + scale * 9 / 10}, 3);
        g.fillPolygon(new int[] { x + scale * 3 / 10, x + scale / 2 , x + scale * 7 / 10}, 
                new int[] {y + scale / 2, y + scale / 10, y + scale / 2}, 3);
        
        g.setColor(Color.RED);
        g.fillOval(x + scale * 2 / 5, y + scale * 2 / 5, scale / 5, scale / 5);
        
        g.setColor(Color.white);
        g.fillOval(x + scale * 17 / 40, y + scale * 17 / 40, scale / 15, scale / 15);
    }
}
