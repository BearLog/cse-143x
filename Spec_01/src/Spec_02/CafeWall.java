
package Spec_02;

import java.awt.*;

// Thomas Bernardi
// 10/7/16
// CSE143X
// TA: Caitlin E. Schaefer
// Assignment #2: Cafe Wall

//This class is written so that any number of square grids can be written at any position
//Each grid is composed of subunits with a black square containing a blue x, and a white square to
//the right. All subunits in a given grid are the same size. In aach grid, the number of rows and
//columns is the same. The even rows of the grids can be offset to the right by any given amount.
public class CafeWall {
    public static final int MORTAR = 2;
    
    public static void main(String[] args){
        DrawingPanel panel = new DrawingPanel(650,400);
        panel.setBackground(Color.GRAY);
        Graphics g = panel.getGraphics();
        
        drawRow(0,0,4,20,g);
        drawRow(50,70,5,30,g);
        drawGrid(10, 150, 4, 25, 0, g);
        drawGrid(250, 200, 3, 25, 10, g);
        drawGrid(425, 180, 5, 20, 10, g);
        drawGrid(400, 20, 2, 35, 35, g);
    }
    
    //this will draw a row of the given figure (black square box, blue x, white square box to the 
    //right) at position (x, y), repeated numPairs times to the right where each square has 
    //side length boxSize
    public static void drawRow(int x, int y, int numPairs, int boxSize, Graphics g){                
        for(int i = 0; i < numPairs; i++){
            g.setColor(Color.BLACK);
            g.fillRect(x + evenPoint(i, boxSize), y, boxSize, boxSize);
            
            g.setColor(Color.BLUE);
            g.drawLine(x + evenPoint(i, boxSize), y, x + oddPoint(i, boxSize), y + boxSize);
            g.drawLine(x + evenPoint(i, boxSize), y + boxSize, x + oddPoint(i, boxSize), y);
            
            g.setColor(Color.WHITE);
            g.fillRect(x + oddPoint(i, boxSize), y, boxSize, boxSize);
        }
        
    }
    
    //this will stack the previously described rows on top of each other so there is a buffer size 
    //MORTAR between each row, so that each even row is offset to the right distance "offset" and 
    //so that it is located at (x, y)
    public static void drawGrid(int x, int y, int numPairs, int boxSize, int offset, Graphics g){        
        for(int i = 0; i < numPairs; i++){
            drawRow(x, y + evenPoint(i, boxSize + MORTAR), numPairs, boxSize, g);
            drawRow(x + offset, y + oddPoint(i, boxSize + MORTAR), numPairs, boxSize, g);
        }
    }
    
    //returns the nth odd multiple of the period where n = iteration
    public static int oddPoint(int iteration, int period){
        return ((2 * iteration + 1 )* period);
    }
    
    //returns the nth even multiple of the period where n / iteration
    public static int evenPoint(int iteration, int period){
        return 2 * iteration * period;
    }
}
